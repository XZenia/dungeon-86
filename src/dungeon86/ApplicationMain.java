package dungeon86;

import javax.swing.JFrame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import asciiPanel.AsciiPanel;
import dungeon86.screens.screen;
import dungeon86.screens.startScreen;

public class ApplicationMain extends JFrame implements KeyListener{
	private static final long serialVersionUID = 1060623638149583738L;
	private AsciiPanel terminal;
    private screen screen;
	public ApplicationMain(){
		super("Peripeteia: A Roguelike Adventure");
		terminal = new AsciiPanel();
		//terminal.write("Dungeon '86",1,1);
		add(terminal);
		setVisible(true);
		pack();
	    screen = new startScreen();
		addKeyListener(this);
		repaint();
	}
	public void repaint(){
		terminal.clear();
		screen.displayOutput(terminal);
		super.repaint();
		
	}
	public void keyPressed(KeyEvent e){
		screen = screen.respondToUserInput(e);
		repaint();
	}
	public void keyReleased(KeyEvent e){
		
	}
	public void keyTyped(KeyEvent e){
		
	}

	public static void main (String [] args) {
	  ApplicationMain close = new ApplicationMain();
	  close.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
