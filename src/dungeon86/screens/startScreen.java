package dungeon86.screens;
import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class startScreen implements screen {
	
	public void displayOutput (AsciiPanel terminal){
	  terminal.writeCenter("Peripeteia: A Roguelike Adventure",3);
	  terminal.writeCenter(" Press [Enter] to enter dungeon ",22);
	}
	public screen respondToUserInput (KeyEvent key){
		return key.getKeyCode() == KeyEvent.VK_ENTER ? new playScreen() : this;
		
	}
	
}
