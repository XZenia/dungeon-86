package dungeon86.screens;
import java.awt.event.KeyEvent;
import java.awt.Color;

import asciiPanel.AsciiPanel;

import dungeon86.World;
import dungeon86.WorldBuilder;
import dungeon86.Creature;
import dungeon86.FieldOfView;
import dungeon86.stuffFactory;
import dungeon86.Tile;
import dungeon86.Item;
import java.util.List;


import java.util.ArrayList;
public class playScreen implements screen {
    private World world;
	private int screenWidth;
	private int screenHeight;
	private Creature player;
	private List<String> messages;
	private FieldOfView fov;
	private screen subscreen;
	
    
	public playScreen(){
		screenWidth = 80;
		screenHeight = 23;
		messages = new ArrayList<String>();
		createWorld();
		fov = new FieldOfView(world);
		
		stuffFactory factory = new stuffFactory(world);
		createCreatures(factory);
		createItems(factory);
	}
	
	private void createCreatures(stuffFactory factory){
	    player = factory.newPlayer(messages,fov);
	    
	    for(int z = 0; z < world.depth(); z++){
	      for (int i = 0; i < 8; i++){
	        factory.newSnake(z,player);
	      }
	      for (int i = 0; i < 7; i++){
	    	  factory.newBat(z);
	      }
	      
	      for (int i = 0; i < z * 2; i++){
	          factory.newZombie(z, player);
	          factory.newGoblin(z, player);
	          factory.newhellknights(z, player);
	      }

	}
}
	
	private void displayMessages(AsciiPanel terminal, List<String> messages) {
	    int top = screenHeight - messages.size();
	    for (int i = 0; i < messages.size(); i++){
	        terminal.writeCenter(messages.get(i), top + i);
	    }
	    messages.clear();
	}
	
	public void displayOutput(AsciiPanel terminal){
		  int left = getScrollX();
		  int top = getScrollY();
		  displayTiles(terminal,left,top);
		  displayMessages(terminal,messages);
		  terminal.write(player.glyph(), player.x - left, player.y - top, player.color());	
		  
		  String stats = String.format(" %3d/%3d HP %8s", player.hp(), player.maxHP(), hunger());
		  terminal.write(stats, 1, 23);
		  
		  if (subscreen != null)
			    subscreen.displayOutput(terminal);
	}
	
	private String hunger(){
	    if (player.food() < player.maxFood() * 0.1)
	        return "Starving";
	    else if (player.food() < player.maxFood() * 0.2)
	        return "Hungry";
	    else if (player.food() > player.maxFood() * 0.9)
	        return "Stuffed";
	    else if (player.food() > player.maxFood() * 0.8)
	        return "Full";
	    else
	        return "";
	}
	
	public screen respondToUserInput(KeyEvent key) {
        int level = player.level();
		if (subscreen != null){
        	subscreen = subscreen.respondToUserInput(key);
        }
        else {
		  switch (key.getKeyCode()){
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_H: player.moveBy(-1, 0, 0); 
            break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_L: player.moveBy( 1, 0, 0); 
            break;
            case KeyEvent.VK_UP:
            case KeyEvent.VK_K: player.moveBy( 0,-1, 0); 
            break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_J: player.moveBy( 0, 1, 0); 
            break;
            case KeyEvent.VK_Y: player.moveBy(-1,-1, 0); 
            break;
            case KeyEvent.VK_U: player.moveBy( 1,-1, 0); 
            break;
            case KeyEvent.VK_B: player.moveBy(-1, 1, 0); 
            break;
            case KeyEvent.VK_N: player.moveBy( 1, 1, 0);break;
            case KeyEvent.VK_E: subscreen = new EatScreen(player); break;
            case KeyEvent.VK_D: subscreen = new DropScreen(player); break;
            case KeyEvent.VK_W: subscreen = new EquipScreen(player); break;
            case KeyEvent.VK_X: subscreen = new ExamineScreen(player); break;
            case KeyEvent.VK_SEMICOLON: subscreen = new LookScreen(player, "Looking", 
					player.x - getScrollX(), 
					player.y - getScrollY()); break;
            case KeyEvent.VK_T: subscreen = new ThrowScreen(player,
                    player.x - getScrollX(),
                    player.y - getScrollY()); break;
            case KeyEvent.VK_F:
                if (player.weapon() == null || player.weapon().rangedAttackValue() == 0)
                 player.notify("You don't have a ranged weapon equipped");
                else
                 subscreen = new FireWeaponScreen(player,
                     player.x - getScrollX(),
                     player.y - getScrollY()); break;
            case KeyEvent.VK_Q: subscreen = new QuaffScreen(player); break;
     
            
         }
         switch (key.getKeyChar()){
         case 'g':
         case ',': player.pickup(); break;
         case '<': 
			   if (userIsTryingToExit())
				   return userExits();
			   else 
				   player.moveBy(0, 0, -1); break;
		 case '>': player.moveBy( 0, 0, 1); break;
		 case '/': subscreen = new HelpScreen(); break;
         }
       }

		  if (player.hp() < 1)
			 return new LoseScreen();
		  if (player.level() > level)
		      subscreen = new LevelUpScreen(player, player.level() - level);
		  if (subscreen == null)
			  world.update();
		  return this; 
   }
	
	
	private boolean userIsTryingToExit(){
		return player.z == 0 && world.tile(player.x, player.y, player.z) == Tile.STAIRS_UP;
	}
	
	private screen userExits(){
	    for (Item item : player.inventory().getItems()){
	        if (item != null && item.name().equals("Amethyst of Noraa"))
	            return new WinScreen();
	    }
	    return null;
	}

	

	private void createWorld(){
		world = new WorldBuilder(90,32,5).makeCaves().build();
	}
	public int getScrollX(){
		return Math.max(0, Math.min(player.x - screenWidth / 2, world.width() - screenWidth));
	}
	public int getScrollY(){
		return Math.max(0, Math.min(player.y - screenHeight / 2, world.height() - screenHeight));
	}
	private void displayTiles(AsciiPanel terminal, int left, int top) {
		fov.update(player.x, player.y, player.z, player.visionRadius());
		 for (int x = 0; x < screenWidth; x++){
		        for (int y = 0; y < screenHeight; y++){
		            int wx = x + left;
		            int wy = y + top;

		            if (player.canSee(wx, wy, player.z))
		                terminal.write(world.glyph(wx, wy, player.z), x, y, world.color(wx, wy, player.z));
		            else
		                terminal.write(fov.tile(wx, wy, player.z).glyph(), x, y, Color.darkGray);
		        }
		    }
		 }
	private void createItems(stuffFactory factory) {
	    for (int z = 0; z < world.depth(); z++){
	        for (int i = 0; i < world.width() * world.height() / 20; i++){
	            factory.newRock(z);
	        }
	        for (int i = 0; i < world.width() * world.height() / 500; i++){
	        	factory.newApple(z);
	        }
	        for (int i = 0; i < world.width() * world.height() / 500; i++){
	        	factory.randomWeapon(z);
	        }
	        for (int i = 0; i < world.width() * world.height() / 800; i++){
	        	factory.randomArmor(z);
	        }
	        for (int i = 0; i < world.width() * world.height() / 500; i++){
	        	factory.randomPotion(z);
	        }
	        
	    }
	    
	    factory.newVictoryItem(world.depth() - 1);
	}
	
}
