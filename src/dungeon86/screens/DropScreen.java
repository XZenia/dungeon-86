package dungeon86.screens;

import dungeon86.Creature;
import dungeon86.Item;
public class DropScreen extends InventoryBasedScreen {
	public DropScreen(Creature player) {
	     super(player);
	  }
	protected String getVerb() {
        return "drop";
    }
	protected boolean isAcceptable(Item item) {
        return true;
    }
	protected screen use(Item item) {
        player.drop(item);
        return null;
    }


}
