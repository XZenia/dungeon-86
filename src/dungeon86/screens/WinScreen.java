package dungeon86.screens;
import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class WinScreen implements screen {

    public void displayOutput(AsciiPanel terminal) {
        terminal.writeCenter("You have successfully escaped the cave with the Amethyst of Noraa...", 1);
        terminal.writeCenter("But a time loop prevents you from escaping...",5);
        terminal.writeCenter("-- press [Enter] to restart --", 22);
    }

    public screen respondToUserInput(KeyEvent key) {
        return key.getKeyCode() == KeyEvent.VK_ENTER ? new playScreen() : this;
    }
}
