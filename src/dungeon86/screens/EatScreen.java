package dungeon86.screens;

import dungeon86.Creature;
import dungeon86.Item;
public class EatScreen extends InventoryBasedScreen {
  public EatScreen(Creature player){
	  super(player);
  }

  protected String getVerb() {
      return "eat";
  }

  protected boolean isAcceptable(Item item) {
      return item.foodValue() != 0;
  }

  protected screen use(Item item) {
      player.eat(item);
      return null;
  }
}

