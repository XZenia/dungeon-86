package dungeon86.screens;

import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class HelpScreen implements screen {
    @Override
    public void displayOutput(AsciiPanel terminal) {
	        terminal.clear();
	        terminal.writeCenter("Peripeteia: A Roguelike Adventure Help", 1);
	        terminal.write("Objective: Descend the caves of great danger, find the lost Amethyst of Noraa,", 1, 3);
	        terminal.write("and return to the surface to win. Use what you find to avoid dying.", 1, 4);
	    
	        int y = 6;
	        terminal.write("[g] or [,] to pick up items", 2, y++);
	        terminal.write("[d] to drop", 2, y++);
	        terminal.write("[e] to eat", 2, y++);
	        terminal.write("[w] to wear or wield", 2, y++);
	        terminal.write("[/] for help", 2, y++);
	        terminal.write("[x] to examine your items", 2, y++);
	        terminal.write("[;] to look around", 2, y++);
	        terminal.write("[f] to fire any ranged weapon you have equipped",2,y++);
	        terminal.write("[t] to throw an item",2,y++);
	        terminal.write("[q] to quaff or drink a potion",2,y++);
	        terminal.write("[>] or [<] to get up on the next or previous floor while standing on it",2,y++);
	        terminal.write("[Esc] to close any open subscreens",2,y++);
	        
	        terminal.writeCenter("-- Press any key to continue --", 22);
	    }
        
        @Override
	    public screen respondToUserInput(KeyEvent key) {
	        return null;
	    }
	}

