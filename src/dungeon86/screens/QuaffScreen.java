package dungeon86.screens;
import dungeon86.Creature;
import dungeon86.Item;

public class QuaffScreen extends InventoryBasedScreen {

        public QuaffScreen(Creature player) {
                super(player);
        }

        protected String getVerb() {
                return "quaff";
        }

        protected boolean isAcceptable(Item item) {
                return item.quaffEffect() != null;
        }

        protected screen use(Item item) {
                player.quaff(item);
                return null;
        }
}