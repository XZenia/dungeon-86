package dungeon86.screens;
import java.awt.event.KeyEvent;
import asciiPanel.AsciiPanel;

public class LoseScreen implements screen {

    public void displayOutput(AsciiPanel terminal) {
        terminal.writeCenter("You are dead", 1);
        terminal.writeCenter("You have been added in the list of poor souls who have perished in this cave", 5);
        
        terminal.writeCenter("-- press [enter] to restart --", 22);
    }

    public screen respondToUserInput(KeyEvent key) {
        return key.getKeyCode() == KeyEvent.VK_ENTER ? new playScreen() : this;
    }
}
