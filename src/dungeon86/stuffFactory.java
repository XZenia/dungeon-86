package dungeon86;
import java.util.List;
import asciiPanel.AsciiPanel;
public class stuffFactory {
   private World world;  
   public stuffFactory(World world){
	   this.world = world;
   }
 
   public Creature newPlayer(List<String> messages, FieldOfView fov){
	  Creature player = new Creature(world, '@', AsciiPanel.brightWhite,"player",100,12,15);
	  world.addAtEmptyLocation(player,0);
	  new PlayerAI(player,messages,fov);
	  return player;
	}
   public Creature newBat(int depth){
	    Creature bat = new Creature(world, 'b', AsciiPanel.white,"bat",12,8,9);
	    world.addAtEmptyLocation(bat,depth);
	    new BatAI(bat);
	    return bat;
	}
   public Creature newSnake(int depth, Creature player){
	   Creature snake = new Creature (world, 's',AsciiPanel.green,"serpent",12,9,9);
	   world.addAtEmptyLocation(snake,depth);
	   new SnakeAI(snake,player);
	   return snake;
   }
   public Creature newZombie(int depth, Creature player){
	   Creature zombie = new Creature (world, 'z',AsciiPanel.brightRed,"zombie",30,16,20);
	   world.addAtEmptyLocation(zombie,depth);
	   new ZombieAI(zombie,player);
	   return zombie;
   }
   
   public Creature newGoblin(int depth, Creature player){
       Creature goblin = new Creature(world, 'g', AsciiPanel.brightGreen, "goblin", 66, 15, 10);
       goblin.equip(randomWeapon(depth));
       goblin.equip(randomArmor(depth));
       goblin.equip(randomPotion(depth));
       world.addAtEmptyLocation(goblin, depth);
       new GoblinAI(goblin, player);
       return goblin;
   }
   public Creature newhellknights(int depth, Creature player){
	   Creature hellknight = new Creature(world, 'H', AsciiPanel.brightRed, "Hell Knight", 70, 20, 15);
       hellknight.equip(randomWeapon(depth));
       hellknight.equip(randomArmor(depth));
       world.addAtEmptyLocation(hellknight, depth);
       new GoblinAI(hellknight, player);
       return hellknight;
   }
   
   public Item newRock(int depth){
       Item rock = new Item(',', AsciiPanel.yellow, "rock");
       rock.modifyAttackValue(1);
       rock.modifyRangedAttackValue(4);
       world.addAtEmptyLocation(rock, depth);
       return rock;
   }
   public Item newApple(int depth){
	   Item apple = new Item('#', AsciiPanel.red, "delicious fruit");
	   world.addAtEmptyLocation(apple, depth);
	   apple.modifyFoodValue(100); 
	   return apple;
   }
   public Item newPotionOfHealth(int depth){
		Item item = new Item('!', AsciiPanel.white, "health potion");
		item.setQuaffEffect(new Effect(1){
			public void start(Creature creature){
				if (creature.hp() == creature.maxHP())
					return;
				
				creature.modifyHP(15);
				creature.doAction("feel your wounds heal as you drink");
			}
		});
		
		world.addAtEmptyLocation(item, depth);
		return item;
	}
   public Item newPotionOfHunger(int depth){
	    Item item = new Item('!', AsciiPanel.white, "bottle of Rugby");
	    item.setQuaffEffect(new Effect(20){
	        public void start(Creature creature){
	            creature.modifyFood(100);
	            creature.doAction("feel less hungry");
	        }
	        public void end(Creature creature){
	            creature.modifyFood(-100);
	            creature.doAction("you feel the effects wear off");
	        }
	    });
	                
	    world.addAtEmptyLocation(item, depth);
	    return item;
	}
   
   public Item newPoison(int depth){
	    Item item = new Item('!', AsciiPanel.white, "bottle of Witch blood");
	    item.setQuaffEffect(new Effect(20){
	        public void start(Creature creature){
	            creature.doAction("feel woozy");
	            creature.modifyAttackValue(5);
	            creature.modifyDefenseValue(5);
	        }
	                        
	        public void update(Creature creature){
	            super.update(creature);
	            creature.modifyHP(-1);
	            
	        }
	    });
	                
	    world.addAtEmptyLocation(item, depth);
	    return item;
	}
   
   
   public Item newPotionOfWarrior(int depth){
	    Item item = new Item('!', AsciiPanel.white, "bottle of Skooma");
	    item.setQuaffEffect(new Effect(20){
	        public void start(Creature creature){
	            creature.modifyAttackValue(5);
	            creature.modifyDefenseValue(5);
	            creature.doAction("feel stronger");
	        }
	        public void end(Creature creature){
	            creature.modifyAttackValue(-5);
	            creature.modifyDefenseValue(-5);
	            creature.doAction("feel a bad hangover");
	        }
	    });
	                
	    world.addAtEmptyLocation(item, depth);
	    return item;
	}
   
   public Item newPotionofWarding(int depth){
	   Item item = new Item('!', AsciiPanel.white, "Potion of Warding");
		item.setQuaffEffect(new Effect(20){
			public void start(Creature creature){
				creature.modifyVisionRadius(3);
				creature.doAction("feel more perceptive");
			}
			public void end(Creature creature){
				creature.modifyVisionRadius(-3);
				creature.doAction("feel normal");
			}
		});
		
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	public Item newPotionOfExperience(int depth){
		Item item = new Item('!', AsciiPanel.white, "experience potion");
		item.setQuaffEffect(new Effect(20){
			public void start(Creature creature){
				creature.doAction("feel more experienced");
				creature.modifyXp(creature.level() * 5);
			}
		});
		
		world.addAtEmptyLocation(item, depth);
		return item;
	}
   
   public Item newVictoryItem(int depth){
       Item item = new Item('*', AsciiPanel.brightWhite, "Amethyst of Noraa");
       AsciiPanel terminal = new AsciiPanel();
       terminal.clear();
       terminal.writeCenter("ESCAPE TO THE FIRST FLOOR!", 5);
       terminal.clear();
       world.addAtEmptyLocation(item, depth);
       return item;
   }
   
   public Item newDagger(int depth){
	    Item item = new Item(')', AsciiPanel.white, "dagger");
	    item.modifyAttackValue(5);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item newSword(int depth){
	    Item item = new Item(')', AsciiPanel.brightWhite, "broadsword");
	    item.modifyAttackValue(10);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item newStaff(int depth){
	    Item item = new Item(')', AsciiPanel.yellow, "Bo staff");
	    item.modifyAttackValue(5);
	    item.modifyDefenseValue(3);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item newLightArmor(int depth){
	    Item item = new Item('[', AsciiPanel.green, "kawaii hoodie");
	    item.modifyDefenseValue(2);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item newMediumArmor(int depth){
	    Item item = new Item('[', AsciiPanel.white, "Dead Man's Plate");
	    item.modifyDefenseValue(4);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }

	  public Item newHeavyArmor(int depth){
	    Item item = new Item('[', AsciiPanel.brightWhite, "Kevlar with Helmet");
	    item.modifyDefenseValue(6);
	    world.addAtEmptyLocation(item, depth);
	    return item;
	  }
	  
	  public Item newBow(int depth){
	        Item item = new Item(')', AsciiPanel.yellow, "short bow");
	        item.modifyAttackValue(2);
	        item.modifyRangedAttackValue(8);
	        world.addAtEmptyLocation(item, depth);
	        return item;
	   }
	  
	  public Item newRifle (int depth){
		  Item item = new Item(')', AsciiPanel.brightGreen,"M1 Garand");
		  item.modifyAttackValue(3);
		  item.modifyRangedAttackValue(12);
		  world.addAtEmptyLocation(item,depth);
		  return item;
	  }
	  
	  public Item newPistol (int depth){
		  Item item = new Item(')', AsciiPanel.brightCyan,"Desert Eagle");
		  item.modifyAttackValue(2);
		  item.modifyRangedAttackValue(10);
		  world.addAtEmptyLocation(item,depth);
		  return item;
	  }
	  

	  public Item randomWeapon(int depth){
	    switch ((int)(Math.random() * 6)){
	    case 0: return newDagger(depth);
	    case 1: return newSword(depth);
	    case 2: return newBow(depth);
	    case 3: return newPistol(depth);
	    case 4: return newRifle(depth);
	    default: return newStaff(depth);
	    }
	  }

	  public Item randomArmor(int depth){
	    switch ((int)(Math.random() * 3)){
	    case 0: return newLightArmor(depth);
	    case 1: return newMediumArmor(depth);
	    default: return newHeavyArmor(depth);
	    }
	  }
	  public Item randomPotion(int depth){
          switch ((int)(Math.random() * 6)){
          case 0: return newPotionOfHealth(depth);
          case 1: return newPoison(depth);
          case 2: return newPotionofWarding(depth);
          case 3: return newPotionOfExperience(depth);
          case 4: return newPotionOfHunger(depth);
          default: return newPotionOfWarrior(depth);
          }
  }

   
}
