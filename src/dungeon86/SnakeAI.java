package dungeon86;
import dungeon86.Creature;
import java.util.List;
import dungeon86.CreatureAI;
public class SnakeAI extends CreatureAI {
	private Creature player;
	
	public SnakeAI (Creature creature, Creature player){
		super(creature);
		this.player = player;
	}	
	public void onUpdate(){
	      if (creature.canSee(player.x, player.y, player.z))
	          hunt(player);
	  }
	public void hunt(Creature target){
	      List<Point> points = new Path(creature, target.x, target.y).points();
	  
	      int mx = points.get(0).x - creature.x;
	      int my = points.get(0).y - creature.y;
	  
	      creature.moveBy(mx, my, 0);
	}

}
