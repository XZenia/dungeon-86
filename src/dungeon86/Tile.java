package dungeon86;

import java.awt.Color;
import asciiPanel.AsciiPanel;
public enum Tile {
   FLOOR((char)250, AsciiPanel.brightBlue, "A dirt and a rock cave floor."),
   WALL((char)177, AsciiPanel.yellow,"A wall and a rock cave floor."),
   BOUNDS('x',AsciiPanel.brightBlack,"You are going to have a bad time here."),
   STAIRS_DOWN('>', AsciiPanel.white, "A stone staircase that goes down."),
   STAIRS_UP('<', AsciiPanel.white, "A stone staircase that goes up."),
   UNKNOWN (' ', AsciiPanel.white, "Nobody knows...");
  
   private char glyph;
   public char glyph(){
	   return glyph;
   }
   private Color color;
   public Color color() {
	   return color;
   }
   Tile(char glyph, Color color, String details){
	   this.glyph = glyph;
	   this.color = color;
	   this.details = details;
   }
   public boolean isDiggable(){
	   return this == Tile.WALL;
   }
   public boolean isGround(){
       return this!= WALL && this !=BOUNDS;
   }
   private String details;
   public String details(){ return details; }
}
