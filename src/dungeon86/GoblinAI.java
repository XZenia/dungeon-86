package dungeon86;

import java.util.List;

public class GoblinAI extends CreatureAI {
    private Creature player;

    public GoblinAI(Creature creature, Creature player) {
        super(creature);
        this.player = player;
    }

    public void hunt(Creature target){
        List<Point> points = new Path(creature, target.x, target.y).points();
    
        int mx = points.get(0).x - creature.x;
        int my = points.get(0).y - creature.y;
    
        creature.moveBy(mx, my, 0);
    }
    
    public void onUpdate(){
        if (canRangedWeaponAttack(player))
            creature.rangedWeaponAttack(player);
        else if (canThrowAt(player))
            creature.throwItem(getWeaponToThrow(), player.x, player.y, player.z);
        else if (creature.canSee(player.x, player.y, player.z))
            hunt(player);
        else if (canPickup())
            creature.pickup();
        else
            wander();
    }

    
}

